#include "SpriteComponent.h"
#include "Actor.h"
#include "Game.h"

/*
 *
 */
SpriteComponent::SpriteComponent(Actor* owner, int drawOrder)
    : Component(owner), mTexture(nullptr), mDrawOrder(0),
    mTexWidth(0), mTexHeight(0)
{
    mOwner->GetGame()->AddSprite(this);
}

/*
 *
 */
SpriteComponent::~SpriteComponent()
{
    mOwner->GetGame()->RemoveSprite(this);
}

/*
 * Draw function uses SDL_RenderCopyEX because actors havea rotation so that sprite to inherit the 
 * location. X, Y coordinates correspond to the top-left corner of the destination. SDL expects angle in 
 * degrees while actor uses angle in radian. Note that this immplementationonly works for games that has 
 * worlds exactly the size of screen.
 */
void SpriteComponent::Draw(SDL_Renderer* renderer)
{
    if (mTexture)
    {
        SDL_Rect r;
        // Scale the width/height by owner's scale
        r.w = static_cast<int>(mTexWidth * mOwner->GetScale());
        r.h = static_cast<int>(mTexHeight * mOwner->GetScale());
        // Center the rectancle around the position of the owner
        r.x = static_cast<int>(mOwner->GetPosition().x - r.w / 2);
        r.y = static_cast<int>(mOwner->GetPosition().y - r.h / 2);

        // Draw (have to convert angle from radians to degrees, and clockwise to counter)
        SDL_RenderCopyEx(
            renderer,                                           // Render target to draw to
            mTexture,                                           // Texture to Draw
            nullptr,                                            // Source rectangle 
            &r,                                                 // Destination rectangle
            -Math::RadiansToDegrees(mOwner->GetRotation()),     // Convert angle
            nullptr,                                            // Point of Rotation
            SDL_FLIP_NONE                                       // Flip Behavior
        );
    }
}

/*
 * SetTexture function both sets the mTexture member variableand uses SDL_QueryTexture to get the width
 * and height of the texture.
 * Function is called by AnimateSpriteComponent::SetAnimateTextures
 */
void SpriteComponent::SetTexture(SDL_Texture* texture)
{
    mTexture = texture;
    // Get width/height of texture. Query the attributes of a texture
    SDL_QueryTexture(mTexture, nullptr, nullptr, &mTexWidth, &mTexHeight);
}
