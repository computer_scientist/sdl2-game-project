#include "Game.h"
//#include "SDL2/SDL_image.h"
#include "SDL2_image/SDL_image.h"
#include <algorithm>
#include "Actor.h"
#include "SpriteComponent.h"
#include "Ship.h"
#include "Asteroid.h"
#include "Random.h"

/*
 * Constructor
 * New Game object created in main.
 */
Game::Game()
    : mWindow(nullptr), mRenderer(nullptr), mIsRunning(true), mUpdatingActors(false)
{
}


/*
 * Called by main. This function initializes the required SDL elements.
 * When required SDL elements succeeds the LoadData() function is called.
 */
bool Game::Initialize()
{
    // Check if able to initialize SDL library
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_JOYSTICK) != 0)
    {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return false;
    }
    // Create Window and verify if window was created with specified position, dimensions, and flags
    mWindow = SDL_CreateWindow("Game Programming in C++ (Chapter3)", 100, 100, 1024, 764, 0);
    if (!mWindow)
    {
        SDL_Log("Failed to create window: %s", SDL_GetError());
        return false;
    }
    // Create renderer. This function create a 2D rendering context for a window
    mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!mRenderer)
    {
        SDL_Log("Failed to create renderer: %s", SDL_GetError());
        return false;
    }
    // Verify if SDL_image is initialized
    if (IMG_Init(IMG_INIT_PNG) == 0) 
    {
        SDL_Log("Unable to intialize SDL_image: %s", SDL_GetError());
        return false;
    } 
    Random::Init();
    // Call LoadData, GetTicks then return if all has been initialized
    LoadData();
    // The SDL_GetTicks function is used to get the number of milliseconds since the
    // SDL library initialization.
    mTicksCount = SDL_GetTicks();
    return true;
}

/*
 * Loop that is ran from the main.cpp. While mIsRunning is true the loop will continue to run
 */
void Game::RunLoop()
{
    while (mIsRunning)
    {
        ProcessInput();
        UpdateGame();
        GenerateOutput();
    }
}

/*
 *
 */
void Game::AddActor(Actor* actor)
{
    // If updating actors, need to add to pending
    if (mUpdatingActors)
    {
        mPendingActors.emplace_back(actor);
    }
    else
    {
        mActors.emplace_back(actor);
    }
    
}

/*
 *
 */
void Game::RemoveActor(Actor* actor)
{
    // Is it in pending actors?
    auto iter = std::find(mPendingActors.begin(), mPendingActors.end(), actor);
    if (iter != mPendingActors.end())
    {
        // Swap to end of vector and pop off (avoid erase copies)
        std::iter_swap(iter, mPendingActors.end() - 1);
        mPendingActors.pop_back();
    }
    // Is it in actors?
    iter = std::find(mActors.begin(), mActors.end(), actor);
    if (iter != mActors.end())
    {
        // Swap to end of vector and pop off (avoid erase copies)
        std::iter_swap(iter, mActors.end() - 1);
        mActors.pop_back();
    }
}

/*
 * Sprite Component Constructor adds itself to a vector of 
 * sprite components. Ensures that mSprites stays sorted by 
 * draw order.
 */
void Game::AddSprite(SpriteComponent* sprite)
{
    //Find the insertion point in the sorted vector
    // The first element with a higher draw order than me
    int myDrawOrder = sprite->GetDrawOrder();
    auto iter = mSprites.begin();
    for (; iter != mSprites.end(); ++iter)
    {
        if (myDrawOrder < (*iter)->GetDrawOrder())
        {
            break;
        }
    }
    // Inserts element before position of iterator
    mSprites.insert(iter, sprite);
}

/*
 * 
 */
void Game::RemoveSprite(SpriteComponent* sprite)
{
    // (We can't swap because it ruins ordering)
    auto iter = std::find(mSprites.begin(), mSprites.end(), sprite);
    mSprites.erase(iter);
}

/*
 *
 */
void Game::Shutdown()
{
    UnloadData();
    // This function cleans up all dynamically loaded library handles, freeing memory related to 
    // SDL2_image
    IMG_Quit();
    SDL_DestroyRenderer(mRenderer);
    SDL_DestroyWindow(mWindow);
    SDL_Quit();
}

/*
 *
 */
SDL_Texture* Game::GetTexture(const std::string& fileName)
{
    SDL_Texture* tex = nullptr;
    // Is the texture already in map?
    auto iter = mTextures.find(fileName);
    if (iter != mTextures.end())
    {
        tex = iter->second;
    }
    else
    {
        // Load from file
        SDL_Surface* surf = IMG_Load(fileName.c_str());
        if (!surf)
        {
            SDL_Log("Failed to load texture file %s", fileName.c_str());
            return nullptr;
        }
        // Create texture from surface
        tex = SDL_CreateTextureFromSurface(mRenderer, surf);
        SDL_FreeSurface(surf);
        if (!tex)
        {
            SDL_Log("Failed to convert surface to texture for %s", fileName.c_str());
            return nullptr;
        }
        mTextures.emplace(fileName.c_str(), tex);
    }
    return tex;
}

/**
 * The SDL_Event structure is the core of all even handling in SDL. It is a union of all 
 * structures used in SDL. Refer to the SDL documentation for all events handled by SDL_Event
 * https://wiki.libsdl.org/SDL_Event?highlight=%28%5CbCategoryStruct%5Cb%29%7C%28SDLStructTemplate%29
 * Called by the RunLoop() function
 */
void Game::ProcessInput()
{
    SDL_Event event;
    // SDL_PollEvent() function takes a pointer to an SDL_Event structure that is to be filled
    // with the event information. It also removes an event from the queue then the event 
    // information will be placed in the event structure as event.type.
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            // The member SDL_QuitEvent is a member of SDL_Event that is responsible for
            // reporting SDL_QUIT
            case SDL_QUIT:
                mIsRunning = false;
                break;
        }
    }
    // The SDL_GetKeyboardState() function gets a snapshot of the current state of the keyboard.
    const Uint8* state = SDL_GetKeyboardState(NULL);
    // SDL_SCANCODE is an enumeration of the SDL keyboard.
    if (state[SDL_SCANCODE_ESCAPE])
    {
        mIsRunning = false;
    }
    // Set mUpdatingActors to true before the loop to handle an actor or component trying to create 
    // another actor inside ProcessInput.
    mUpdatingActors - true;
    for (auto actor : mActors)
    {
        actor->ProcessInput(keyState);
    }
    mUpdatingActors = false;
}

/*
 *
 */
void Game::UpdateGame()
{
    // Compute delta time
    // Wait until 16 ms has elapsed since last frame
    while (!SDL_TICKS_PASSED(SDL_GetTicks(), mTicksCount + 16));
    float deltaTime = (SDL_GetTicks() - mTicksCount) / 1000.0;
    if (deltaTime > 0.05)
    {
        deltaTime = 0.05;
    }
    mTicksCount = SDL_GetTicks();

    // Update all actors
    mUpdatingActors = true;
    for (auto actor : mActors)
    {
        actor->Update(deltaTime);
    }
    mUpdatingActors = false;

    // Move all pending actors to mActors
    for (auto pending : mPendingActors)
    {
        mActors.emplace_back(pending);
    }
    mPendingActors.clear();

    // Add any dead actors to a temp vector
    std::vector<Actor*> deadActors;
    for (auto actor : mActors)
    {
        if (actor->GetState() == Actor::EDead)
        {
            deadActors.emplace_back(actor);
        }
    }

    // Delete dead actors (which removes them from mActors)
    for (auto actor : deadActors)
    {
        delete actor;
    }
}

/*
 *
 */
void Game::GenerateOutput()
{
    SDL_SetRenderDrawColor(mRenderer, 220, 220, 220, 255);
    SDL_RenderClear(mRenderer);
    // Draw all sprite components
    for (auto sprite : mSprites)
    {
        sprite->Draw(mRenderer);
    }
    SDL_RenderPresent(mRenderer);
}

/*
 *
 */
void Game::UnloadData()
{
    // Delete Actors
    // Because ~Actor calls RemoveActor, use a different style loop
    while (!mActors.empty())
    {
        delete mActors.back();
    }
    // Destroy textures
    for (auto i : mTextures)
    {
        SDL_DestroyTexture(i.second);
    }
    mTextures.clear();
}

/*
 * Creates actors such as the background which takes two components
 * which has different scroll speeds that create a parallax effect.
 * Called by Game::Initialize()
 */
void Game::LoadData()
{
    // Create player's ship
    mShip = new Ship(this);
    mShip->SetPosition(Vector2(512.0, 384.0));
    mShip->SetRotation(Math::TauOver4);

    // Create asteriods
    const int numAsteroids = 20;
    for (int i = 0; i < numAsteroids; i++)
    {
        new Asteroid(this);
    }
}

void Game::AddAsteroid(Asteroid* ast)
{
    mAsteroids.emplace_back(ast);
}

void Game::RemoveAsteroid(Asteroid* ast)
{
    auto iter = std::find(mAsteroids.begin()), mAsteroids.end(), ast);
    if (iter != mAsteroids.end())
    {
        mAsteroids.erase(iter);
    }
}
