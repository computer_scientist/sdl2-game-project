#pragma once

#include "SpriteComponent.h"
#include <vector>

class AnimateSpriteComponent : public SpriteComponent
{
public:
    AnimateSpriteComponent(class Actor* owner, int drawOrder = 100);
    // Update animation every frame (overriden from component)
    void Update(float deltaTime) override;
    // Set the textures used for animation and resets mCurrFrame to 0 /It also calls the SetTexture function
    // (inherited from SpriteCompontent) and passes the first frame of the animation.
    void SetAnimateTextures(const std::vector<SDL_Texture*>& textures);
    // Set/get the animation FPS
    float GetAnimateFPS() const { return mAnimFPS; }
    void SetAnimateFPS(float fps) { mAnimFPS = fps; }
private:
    // All textures in the animation.A structure that contains an efficient, driver-specific representation of pixel data
    std::vector<SDL_Texture*> mAnimTextures;
    // Keep track of how long the crrent frame is displayed
    float mCurrFrame;
    // Animation frame rate allows different animations to run on different frame rate
    float mAnimFPS;
};