#include "AnimateSpriteComponent.h"
#include "Math.h"

AnimateSpriteComponent::AnimateSpriteComponent(Actor* owner, int drawOrder)
    : SpriteComponent(owner, drawOrder), mCurrFrame(0.0), mAnimFPS(24.0)
{    
}

/*
 * First, update mCurrFrame as a function of the animation FPS and delta time,
 * Next, makes sure that mCurrFrame remains less than the number of textures,
 * Finally, cast mCurrFrame to int, grab the correct texture from mAnimTextures, and 
 * call SetTexture.
 */
void AnimateSpriteComponent::Update(float deltaTime)
{
    SpriteComponent::Update(deltaTime);

    if (mAnimTextures.size() > 0)
    {
        // Update the current frame based on frame rate and delta time
        mCurrFrame += mAnimFPS * deltaTime;
        // Wrap current frame if needed
        while (mCurrFrame >= mAnimTextures.size())
        {
            mCurrFrame -= mAnimTextures.size();
        }
    }
    // Set the current texture
    SetTexture(mAnimTextures[static_cast<int>(mCurrFrame)]);
}

/*
 * Takes in a vector of SDL_Texture pointer, assign the class vector of SDL_texture.
 * SDL_Texture is a structure that contains an efficient, driver-specific representation of pixel data 
 */
void AnimateSpriteComponent::SetAnimateTextures(const std::vector<SDL_Texture*>& textures)
{
    mAnimTextures = textures;
    if (mAnimTextures.size() > 0)
    {
        // Set the active texture to first frame
        mCurrFrame = 0.0;
        SetTexture(mAnimTextures[0]);
    }
}