#include "MoveComponent.h"
#include "Actor.h"

MoveComponent::MoveComponent(class Actor* owner, int updateOrder)
    : Component(owner, updateOrder),
    mAngularSpeed(0.0),
    mForwardSpeed(0.0)
    {
    }

void MoveComponent::Update(float deltaTime)
{
    if (!Math::NearZero(mAngularSpeed))
    {
        float rot = mOwner->GetRotation();
        rot += mAngularSpeed * deltaTime;
        mOwner->SetRotation(rot);
    }
    if (!Math::NearZero(mForwardSpeed))
    {
        Vector2 pos = mOwner->GetPosition();
        pos += mOwner->GetForward() * mForwardSpeed * deltaTime;
        // Screen wrapping code only for asteriods
        if (pos.x < 0.0)
        {
            pos.x = 1022.0;
        }
        else if (pos.x > 1024.0)
        {
            pos.x = 2.0;
        }
        if (pos.y < 0.0)
        {
            pos.y = 766.0;
        }
        else if (pos.y > 766.0)
        {
            pos.y = 2.0;
        }
        mOwner->SetPosition(pos);
    }
}