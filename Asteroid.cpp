#include "Asteroid.h"
#include "SpriteComponent.h"
#include "MoveComponent.h"
#include "Game.h"
#include "Random.h"
#include "CircleComponent.h"

Asteroid::Asteroid(Game* game)
    : Actor(game), mCircle(nullptr)
{
    // Initialize to random position/orientation
    Vector2 randPos = Random::GetVector(Vector2::Zero, Vector2(1024.0, 768.0));
    SetPosition(randPos);
    SetRotation(Random::GetFloatRange(0.0, Math::Tau));

    // Create a sprite component
    SpriteComponent* sc = new SpriteComponent(this);
    sc->SetTexture(game->GetTexture("Assets/Asteroid.png"));

    // Create a move component, and set a forward speed
    MoveComponent* mc = new MoveComponent(this);
    mc->SetForwardSpeed(150.0);

    // Create a circle component (for collision)
    mCircle = new CircleComponent(this);
    mCircle->SetRadius(40.0);

    // Add to mAsteriods in game
    game-AddAsteriod(this);
}

Asteroid::~Asteroid()
{
    GetGame()->RemoveAsteriod(this);
}